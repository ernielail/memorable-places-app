//
//  ViewController.swift
//  Memorable Places
//
//  Created by Ernie Lail on 1/18/18.
//  Copyright © 2018 Ernie Lail. All rights reserved.
//

import UIKit

import MapKit


protocol sendData {
    
    func dataReady(data:[String:Any])
}

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    public var annotationFromLoad = MKPointAnnotation()
    
    
    @IBOutlet var myTF3: UITextField!
    
    @IBOutlet var saveBtn: UIButton!
    
    @IBOutlet var locLabel: UILabel!
    
    var locUpdated = false;
    
    var delegate:sendData? = nil
    
    @IBOutlet var map: MKMapView!
    
    var locationManager = CLLocationManager()
    
    var dataStore: [String : Any] = [:]
    
    var newIndex:Int = 0
    
    var myname = ""
    
    var mylattitude:CLLocationDegrees = 0
    
    var mylongitude:CLLocationDegrees = 0
    
    let currentLoc = MKPointAnnotation()
    
    let destLoc = MKPointAnnotation()
    
    @IBAction func returnDirections(_ sender: Any) {
        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        
        let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(mylattitude), longitude: CLLocationDegrees(mylongitude))
        
        let location = MKCoordinateRegion(center: coordinate, span: span)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = self.myname
        
        self.map.addAnnotation(annotation)
        
        let request = MKDirectionsRequest()
        let currPlacemark = MKPlacemark(coordinate: currentLoc.coordinate)
        let currItem = MKMapItem(placemark: currPlacemark)
        request.source = currItem

        let destPlacemark = MKPlacemark(coordinate: destLoc.coordinate)
        let destItem = MKMapItem(placemark: destPlacemark)
        request.destination = destItem
        request.requestsAlternateRoutes = false
        
        let directions = MKDirections(request: request)
        
        directions.calculate(completionHandler: {(response, error) in
            if error != nil {
                print("Error getting directions")
            } else {
                self.showRoute(response!)
            }
        })
    }
    
    
    
    func showRoute(_ response: MKDirectionsResponse) {
        for route in response.routes {
            map.add(route.polyline,
                    level: MKOverlayLevel.aboveRoads)
            for step in route.steps {
                print(step.instructions)
            }
        }
    }
    
    
    @IBAction func btnPress(_ sender: Any) {
        if(myTF3.text != ""){
            print("Got Saved Data Name")
            self.myname = myTF3.text!
            
            if(self.myname != ""){
                
                print("Variables are in place, Creating new Dictionary")
                
                let dataStore = ["lat":mylattitude,"long":mylongitude,"name":self.myname] as [String : Any]
                
                delegate?.dataReady(data: dataStore)
            }
            else{
                print("Text Field Was Blank!!!")
            }
        }
        
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation:CLLocation = locations[0]
        
        let lattitude = userLocation.coordinate.latitude
        
        let longitude = userLocation.coordinate.longitude
        
        //zoom
        let latDelta:CLLocationDegrees = 0.005
        let longDelta:CLLocationDegrees = 0.005
        
        //span from zoom
        let span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
        
        let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(lattitude), longitude: CLLocationDegrees(longitude))
        
        //region from location var and span var
        let region:MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
        
        self.map.setRegion(region, animated: true)
        currentLoc.coordinate = location
        currentLoc.title = "Current Location"
        locationManager.stopUpdatingLocation()
        
        if mylattitude != 0 {
            map.showsUserLocation = false;
            let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
            let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(mylattitude), longitude: CLLocationDegrees(mylongitude))
            let region = MKCoordinateRegion(center: coordinate, span: span)
            self.map.setRegion(region, animated: true)
            destLoc.coordinate = coordinate
            destLoc.title = self.myname
            self.map.addAnnotation(destLoc)
          //  self.directBtn.isHidden = false;
            self.map.addAnnotation(currentLoc)
        }
    }
    
    
    @objc func longpress(gestureRecognizer:UIGestureRecognizer){
        print("Long Press!!!")
       // self.directBtn.isHidden = false;
        
        let touchPoint = gestureRecognizer.location(in: self.map)
        
        let coords  = self.map.convert(touchPoint, toCoordinateFrom:self.map)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coords
        annotation.title = "New Location"
        annotation.subtitle = "Added"
        
        let allAnnotations = self.map.annotations
        self.map.removeAnnotations(allAnnotations)
        
        map.addAnnotation(annotation)
        
        self.mylongitude = CLLocationDegrees(coords.longitude)
        self.mylattitude = CLLocationDegrees(coords.latitude)
        
        let userLocation:CLLocation = CLLocation(latitude: self.mylattitude, longitude: self.mylongitude)
        
        CLGeocoder().reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if error != nil{
                print("No Address Available")
            }
            else{
                if  let pm = placemarks?[0]{
                    if   (pm.subThoroughfare  != nil){
                        //street number
                        //print(pm.subThoroughfare)
                        ////street name
                        //print(pm.thoroughfare)
                        //city
                        //print(pm.locality)
                        //state
                        //print(pm.administrativeArea)
                        //zip code
                        // print(pm.postalCode)
                        self.myname = String(pm.subThoroughfare!) + " " + String(pm.thoroughfare!)
                    }
                    else {
                        self.myname = "Longitude: " + String(self.mylongitude) + " / Latitude:" + String(self.mylattitude)
                        print("No Address Available")
                    }
                }
            }
        }
        self.myTF3.text = self.myname;
        self.locLabel.isHidden = false;
        self.myTF3.isHidden = false;
        self.saveBtn.isHidden = false;
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        map.showsUserLocation = true;
        
        //set up gesture recognizer
        let uilpg = UILongPressGestureRecognizer(target: self, action: #selector(ViewController.longpress(gestureRecognizer:)))
        
        //set long press time
        uilpg.minimumPressDuration=2
        
        //        add to map
        map.addGestureRecognizer(uilpg)
        
        //self.directBtn.isHidden = true;
        self.locLabel.isHidden = true;
        self.myTF3.isHidden = true;
        self.saveBtn.isHidden = true;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
}

