//
//  TableViewController.swift
//  Memorable Places
//
//  Created by Ernie Lail on 1/18/18.
//  Copyright © 2018 Ernie Lail. All rights reserved.
//
import UIKit
import MapKit

class TableViewController: UITableViewController, sendData {
    
    func dataReady(data: [String : Any]) {
        print("Data Sent to TableViewController")
        dataArray?.append(data)
        UserDefaults.standard.set(self.dataArray, forKey: "PlacesArray")
        print("dataReceived: \(data)")
        self.table.reloadData()
    }
    
    var dataArray:[[String:Any]]? = []
    var selectedLat:CLLocationDegrees = 0
    var selectedLong:CLLocationDegrees = 0
    var selectedName = ""
    
    @IBOutlet var table: UITableView!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ViewController  {
            destination.delegate = self;
            destination.mylongitude = selectedLong
            destination.mylattitude = selectedLat
            destination.myname = selectedName
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        table.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if ((UserDefaults.standard.object(forKey: "PlacesArray")) != nil){
            print("Got Saved Data! - Now Loading...")
            let tempDict = UserDefaults.standard.object(forKey: "PlacesArray")
            self.dataArray = tempDict as? [[String : Any]]
        }
        else{
            //this is a brand new install or no data saved
            print("No Data - Display Will Be Blank")
        }
        table.reloadData()
    }
    
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if dataArray != nil {
            return (dataArray!.count)
        }
        else{
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            dataArray?.remove(at: indexPath.row)
            UserDefaults.standard.set(dataArray, forKey: "PlacesArray") // UserDefaults.standard() is now UserDefaults.standard
            table.reloadData()
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("Updating Table Field")
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        
        if dataArray != nil {
            // Configure the cell...
            if(dataArray?.count != 0){
                if(dataArray?[indexPath.row ]["name"] != nil){
                    cell.textLabel?.text = dataArray?[indexPath.row]["name"] as? String
                }
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedLong = dataArray?[indexPath.row ]["long"] as! CLLocationDegrees
        selectedLat = dataArray?[indexPath.row ]["lat"] as! CLLocationDegrees
        selectedName = dataArray?[indexPath.row ]["name"] as! String
        performSegue(withIdentifier: "toMap", sender: nil)
    }
    
}
